import java.util.Scanner;
public class TicTacToe{
	public static void main(String[]args){
		
		System.out.println("Welcome to a game of TicTacToe");
		
		Scanner scan = new Scanner(System.in);
		Board theBoard = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		int row;
		int col;
		
		while(gameOver == false){
			System.out.println(theBoard);
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			System.out.println("Place token in row 0? 1? or 2?");
			row = scan.nextInt();
			System.out.println("Place token in col 0? 1? or 2?");
			col = scan.nextInt();

			while(theBoard.placeToken(row, col, playerToken)==false){
				System.out.println("Place token in row 0? 1? or 2?");
				row = scan.nextInt();
				System.out.println("Place token in col 0? 1? or 2?");
				col = scan.nextInt();
			}
			
			if(theBoard.checkIfFull()==true){
				System.out.println("It's a tie!!");
				gameOver=true;
			}
			else if(theBoard.checkIfWinning(playerToken)==true){
				System.out.println("Player "+player+" has won the game");
				gameOver=true;
			}
			else{
				player++;
				if(player>2){
					player = 1;
				}
			}
		}
	}
}