public class Board{
	private Square[][] tictactoeboard;
	
	public Board(){
		this.tictactoeboard = new Square[3][3];
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				this.tictactoeboard[i][j]=Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String display = "";
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				display = display+this.tictactoeboard[i][j]+" ";
			}
			display = display+"\n";
		}
		return display;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(row>2 || row<0 || col>2 || col<0){
			return false;
		}
		else if(this.tictactoeboard[row][col]!=Square.BLANK){
			return false;
		}
		else{
			this.tictactoeboard[row][col]=playerToken;
			return true;
		}
	}
	
	public boolean checkIfFull(){
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				if(this.tictactoeboard[i][j]==Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkWinningHorizontal(Square playerToken){
		int counter = 0;
		for(int i=0; i<3; i++){
			counter=0;
			for(int j=0; j<3; j++){
				if(this.tictactoeboard[i][j]==playerToken){
					counter++;
				}
				if(counter==3){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkWinningVertical(Square playerToken){
		int counter = 0;
		for(int i=0; i<3; i++){
			counter=0;
			for(int j=0; j<3; j++){
				if(this.tictactoeboard[j][i]==playerToken){
					counter++;
				}
				if(counter==3){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if(checkWinningHorizontal(playerToken)==true){
			return true;
		}
		else if(checkWinningVertical(playerToken)==true){
			return true;
		}
		else{
			return false;
		}
	}
}